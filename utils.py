import numpy as np
import tensorflow.keras.backend as K

def f1(y_true, y_pred):
    def allsum(x):
        x = K.flatten(x)
        return K.sum(x)
    c_y_pred = K.cast(K.argmax(y_pred, axis=-1), 'float32')
    c_y_true = K.cast(y_true, 'float32')

    true_positives = allsum(c_y_true * c_y_pred)
    possible_positives = allsum(c_y_true)
    predicted_positives = allsum(c_y_pred)

    recall = true_positives / (possible_positives + K.epsilon())
    precision = true_positives / (predicted_positives + K.epsilon())

    f1_score = 2 * ((precision * recall) / (precision + recall + K.epsilon()))
    return f1_score

def preprocess_imagenet(img):
    img = np.array(img, dtype=np.float32)
    img *= 2. / 255.
    img -= 1
    return img
