pushd data

kaggle competitions download -c cil-road-segmentation-2019

echo "Decompressing downloaded data and removing archives..."
unzip sample_submission.csv.zip
rm sample_submission.csv.zip
unzip training.zip
rm training.zip
tar xvf test_images.tar.gz
rm test_images.tar.gz
echo "Done."

popd data
