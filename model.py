import math
import numpy as np

import tensorflow as tf
from tensorflow.keras import layers
import tensorflow.keras as keras

from basemodel import BaseModel
from patches import sample_batch, generate_patches

from dataset_sequence import process_sat, EvaluationPatches, SampledPatches

class UNetXception(BaseModel):
    def __init__(self, fg_threshold=0.5, rotate_sample=True, num_epochs=2000, batch_size=16, img_size=256,
                 learning_rate=0.0001, model_filename="unet-model.ckpt", device="/device:GPU:0"):
        # threshold to consider a patch foreground at
        # (relative portion of patch marked as foregroun d)
        self.fg_threshold = fg_threshold
        # indicates whether the sampling process should apply rotation to the images/patches
        self.rotate_sample = rotate_sample
        # the number of training patches to fit the linear function to
        self.num_epochs = num_epochs
        # the file to persist the trained model in
        self.model_filename = model_filename
        # the training batch size
        self.batch_size = batch_size

        self.img_size = int(img_size)

        # active Keras Model instance
        self.model = None

    def build_model(self, model_input_dimensions):
        # note that model_input_dimensions may differ from self.img_size

        inputs = keras.Input(shape=(model_input_dimensions, model_input_dimensions) + (3,))
        norm_inputs = layers.BatchNormalization()(inputs)

        ### [First half of the network: downsampling inputs] ###

        # Entry block
        x = layers.Conv2D(32, 3, strides=2, padding='same')(norm_inputs)
        x = layers.BatchNormalization()(x)
        x = layers.Activation('relu')(x)

        previous_block_activation = x  # Set aside residual

        # Blocks 1, 2, 3 are identical apart from the feature depth.
        for filters in [64, 128, 256]:
            x = layers.Activation('relu')(x)
            x = layers.SeparableConv2D(filters, 3, padding='same')(x)
            x = layers.BatchNormalization()(x)

            x = layers.Activation('relu')(x)
            x = layers.SeparableConv2D(filters, 3, padding='same')(x)
            x = layers.BatchNormalization()(x)

            x = layers.MaxPooling2D(3, strides=2, padding='same')(x)

            # Project residual
            residual = layers.Conv2D(
                filters, 1, strides=2, padding='same')(previous_block_activation)
            x = layers.add([x, residual])  # Add back residual
            previous_block_activation = x  # Set aside next residual

        ### [Second half of the network: upsampling inputs] ###

        previous_block_activation = x  # Set aside residual

        for filters in [256, 128, 64, 32]:
            x = layers.Activation('relu')(x)
            x = layers.Conv2DTranspose(filters, 3, padding='same')(x)
            x = layers.BatchNormalization()(x)

            x = layers.Activation('relu')(x)
            x = layers.Conv2DTranspose(filters, 3, padding='same')(x)
            x = layers.BatchNormalization()(x)

            x = layers.UpSampling2D(2)(x)

            # Project residual
            residual = layers.UpSampling2D(2)(previous_block_activation)
            residual = layers.Conv2D(filters, 1, padding='same')(residual)
            x = layers.add([x, residual])  # Add back residual
            previous_block_activation = x  # Set aside next residual
        
        # per-pixel classification layer
        outputs = layers.Conv2D(
            2, 3, activation='sigmoid', padding='same')(x)
        
        model = keras.Model(inputs, outputs)
        
        return model

    def save(self):
        self.model.save(self.model_filename)

    def restore(self):
        pass # this is done differently in predict and train 

    def train(self, training_data, num_epochs=15, restore=False, validation_data=None, num_patches=14000):
        if restore:
            self.model.load_weights(self.model_filename)
        else:
            self.model = self.build_model(self.img_size)
        
        self.model.compile(optimizer='rmsprop', loss='sparse_categorical_crossentropy')

        # regularly save model checkpoints (weights only)
        callbacks = [
            keras.callbacks.ModelCheckpoint(self.model_filename,
                                            save_weights_only=True,
                                            save_best_only=True)
        ]

        # use patches from validation_data for validation during training (if specified)
        train_gen = SampledPatches(self.img_size, self.batch_size, training_data,
                                   int(num_patches / self.batch_size),
                                   rot_prob=0.3 if self.rotate_sample else 0.0)

        val_gen = EvaluationPatches(self.img_size, self.batch_size, validation_data) if validation_data is not None else None

        self.model.fit(train_gen, epochs=num_epochs, validation_data=val_gen, callbacks=callbacks)

    def predict(self, sat_image_data):
        # determine image dimension of prediction input
        first_img = sat_image_data[0]
        w = first_img.shape[1]
        h = first_img.shape[0]
        assert w == h, "Assuming squared image inputs."

        # construct model
        prediction_model = self.build_model(w)
        # load weights
        prediction_model.load_weights(self.model_filename)

        prediction_input_gen = process_sat(np.array(sat_image_data))
        results = prediction_model.predict(prediction_input_gen)
        
        return results.argmax(axis=-1)
        
