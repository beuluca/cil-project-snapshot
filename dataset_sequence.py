import os
import math
from tensorflow import keras
import numpy as np

from patches import sample_batch, generate_patches

class SampledPatches(keras.utils.Sequence):
    """
    Keras Sequence based on a dataset that consist of sampling
    patches of a specified size (img_size) from the training data set.
    """

    def __init__(self, img_size, batch_size, image_pairs, num_patches, rot_prob=.3, preprocess_sat=None):
        self.batch_size = batch_size
        self.img_size = img_size
        self.image_pairs = image_pairs
        self.num_patches = num_patches

        self.rot_prob = rot_prob

        if preprocess_sat is None:
            preprocess_sat = process_sat
        self.preprocess_sat = preprocess_sat


    def __len__(self):
        return self.num_patches

    def __getitem__(self, idx):
        """Returns tuple (input, target) correspond to batch #idx."""
        rand = np.random.RandomState(int.from_bytes(os.urandom(4), byteorder='little'))
        batch = sample_batch(self.image_pairs, batch_size=self.batch_size, patch_size=self.img_size, rot_prob=self.rot_prob, rand=rand)
        batch_tuple = (self.preprocess_sat(np.array(list(map(lambda p: p[0], batch)))),
                       process_gt(np.array(list(map(lambda p: p[1], batch)))))

        return batch_tuple


class EvaluationPatches(keras.utils.Sequence):
    """
    Keras Sequence based on all image patches of a given size (img_size)
    that can be generated from a given fixed set of image pairs (satellite + groundtruth).
    """

    def __init__(self, img_size, batch_size, image_pairs, preprocess_sat=None):
        self.batch_size = batch_size
        self.img_size = img_size
        self.image_pairs = image_pairs

        if preprocess_sat is None:
            preprocess_sat = process_sat
        self.preprocess_sat = preprocess_sat

        self.image_patches = np.array(
            [generate_patches(image[0], patch_size=self.img_size) for image in image_pairs]
        ).reshape([-1, self.img_size, self.img_size, 3])

        self.gt_patches = np.array(
            [generate_patches(image[1], patch_size=self.img_size)  for image in image_pairs]
        ).reshape([-1, self.img_size, self.img_size])

        self.image_patches = self.preprocess_sat(self.image_patches)
        self.gt_patches = process_gt(self.gt_patches)

        self.num_patches = len(self.image_patches)

    def __len__(self):
        return int(math.ceil(self.num_patches / self.batch_size))

    def __getitem__(self, idx):
        lower = idx * self.batch_size
        upper = (idx+1) * self.batch_size

        return self.image_patches[lower:upper], self.gt_patches[lower:upper]


def process_gt(gt_patches):
    """
    Processing steps applied to groundtruth images before
    they are fed to the model.
    """
    gt_patches = gt_patches / 255.0
    fg_threshold = 0.25

    gt_patches[gt_patches > fg_threshold] = 1
    gt_patches[gt_patches <= fg_threshold] = 0

    return gt_patches

def process_sat(sat_patches):
    """
    Processing steps applied to the satellite images before
    they are fed to the model.
    """
    return sat_patches / 255.0
