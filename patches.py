import math
import numpy as np

import PIL

"""
This file contains utility functions that allow to sample from the training data
in patches (see sample_batch) and to divide larger given images into patches of
a specified size (see generate_patches).
"""


def sample_batch(dataset, batch_size=16, patch_size=128, rot_prob=0.2, noise_prob=0.5, rand=None):
    """
    Samples a batch of training pairs of satellite image patches and the corresponding
    groundtruth segmentation patches.

    :param dataset
        Pair of lists that contain the training images in order of name:
        * dataset[0] should be a list of np.arrays, each representing one satellite image.
        * dataset[1] should be a list of np.arrays, each representing one groundtruth segmentation image.
        * dataset[0][i] is the satellite image corresponding to segmentation image dataset[1][i].
    :param patch_size
        The size of the sampled patches in pixels.
    :param rot_prob
        The probability with which the sampling process should additionally rotate the sampled patch
        by some random rotation angle.
    """
    return [sample_transformed_patch(dataset, patch_size, rot_prob, noise_prob, rand) for i in range(batch_size)]


def generate_patches(sat_image, groundtruth=None, patch_size=128):
    """
    Divides the given sat_image and groundtruth image into patches of
    size patch_size.

    If the images are not evenly dividable into patches of the given size,
    the last column and the last row may overlap with the second-to-last
    row and column respectively.

    :param sat_image
        The satelite image to divide.
    :param groundtruth = None
        The groundtruth image, if known.
    :param patch_size
        The patch size to divide the images into.

    :returns
        If the groundtruth image was given, an aligned list of
        pairs of patches of the satellite image and patches of
        the groundtruth image.

        If the groundtruth image was not given (None), a list of
        patches of the satellite image.
    """
    image_size = sat_image.shape[0]
    num_patches = int(math.ceil(image_size / float(patch_size)))

    sat_patches = []
    groundtruth_patches = []

    for x in range(num_patches):
        for y in range(num_patches):
            # compute initial patch coords using x, y
            patch_left = x * patch_size
            patch_top = y * patch_size

            # make sure the patch does not exceed the image space
            patch_right = min(patch_left + patch_size, image_size)
            patch_left = patch_right - patch_size # make sure patch always fits horizontally
            patch_bottom = min(patch_top + patch_size, image_size)
            patch_top = patch_bottom - patch_size # make sure patch always fits vertically

            sat_patches.append(sat_image[patch_left:patch_right, patch_top: patch_bottom])
            if groundtruth is not None:
                groundtruth_patches.append(groundtruth[patch_left:patch_right, patch_top: patch_bottom])

    if groundtruth is not None:
        return list(zip(sat_patches, groundtruth_patches))
    else:
        return sat_patches

def sample_patch(dataset, patch_size, rand=None):
    """
    Samples a single training pair of a satellite image patch and a corresponding
    groundtruth segmentation patch.

    :param dataset
        The dataset to sample from (see sample_batch for a structural description).
    :param patch_size
        The patch size to sample.
    """
    if rand is None:
        rand = np.random
    img_idx = rand.randint(0, len(dataset))
    image = dataset[img_idx]

    image_size = image[0].shape[0] # assume a squared image here

    # sample random image top-left anchor
    patch_center_x = rand.randint(0, image_size)
    patch_center_y = rand.randint(0, image_size)

    patch_left = patch_center_x - int(patch_size / 2)
    patch_top = patch_center_y - int(patch_size / 2)

    # translate sampled coords into tiled image space
    patch_left = patch_left + image_size
    patch_right = patch_left + patch_size
    patch_top = patch_top + image_size
    patch_bottom = patch_top + patch_size

    patch_center_x = patch_center_x + image_size
    patch_center_y = patch_center_y + image_size

    sat_patch = tile(image[0])[patch_left:patch_right, patch_top:patch_bottom]
    gt_patch = tile(image[1])[patch_left:patch_right, patch_top:patch_bottom]

    return (sat_patch, gt_patch)

def sample_transformed_patch(dataset, patch_size, rot_prob, noise_prob, rand=None):
    """
    Samples a single training pair of a satellite image patch and a corresponding
    groundtruth segmentation patch and applies a rotation at random.

    :param dataset
        The dataset to sample from (see sample_batch for a structural description).
    :param patch_size
        The patch size to sample.
    :param rot_prob
        The probability with which the sampling process should additionally rotate the sampled patch
        by some random rotation angle.
    :param noise_prob
        The probability with which the sampling process should additionally add random noise
        to the sampled satellite image.
    """
    if rand is None:
        rand = np.random.RandomState()
    patch_pair = sample_patch(dataset, patch_size, rand)

    if rand.rand() < rot_prob:
        random_deg = rand.rand(1) * 360.0
        transformed_sat_patch = rotate_crop_image(patch_pair[0], random_deg)
        transformed_gt_patch = rotate_crop_image(patch_pair[1], random_deg)

        assert transformed_sat_patch.shape[0] == patch_size
        assert transformed_gt_patch.shape[0] == patch_size

        patch_pair = (transformed_sat_patch, transformed_gt_patch)

    if rand.rand() < noise_prob:
        patch_pair = (add_pixel_noise(patch_pair[0], rand=rand), patch_pair[1])

    return patch_pair

def tile(img):
    """
    Returns a 3x3 tiled variant of the given image.

    The given image is at the center of the tiled output. The images positioned around it
    are flipped/mirrored in a way, that the transitions between the tiles are seamless.

    """
    w = img.shape[1]
    h = img.shape[0]

    shape = [img.shape[0]*3, img.shape[1]*3, 3] if len(img.shape) > 2 else [img.shape[0]*3, img.shape[1]*3]
    tiled = np.zeros(shape, dtype=np.uint8)

    # center
    tiled[h:2*h, w:2*w] = img

    # left
    tiled[0:h, 0:w] = np.flip(img, axis=[0, 1])
    tiled[h:2*h, 0:w] = np.flip(img, axis=1)
    tiled[2*h:, 0:w] = np.flip(img, axis=[0, 1])

    # right
    tiled[0:h, 2*w:] = np.flip(img, axis=[0, 1])
    tiled[h:2*h, 2*w:] = np.flip(img, axis=1)
    tiled[2*h:, 2*w:] = np.flip(img, axis=[0, 1])

    # top
    tiled[0:h, w:2*w] = np.flip(img, axis=0)
    # bottom
    tiled[2*h:, w:2*w] = np.flip(img, axis=0)

    return tiled

def rotate_crop_image(image, deg):
    """
    Returns the given image rotated *and* cropped.

    The cropping is applied to avoid black borders as
    a result of rotating a square.

    The cropping induces a zoom factor of

    :param image
        The image of dimension [N, N].
    :param deg
        The rotation angle in degree.

    :returns
        The rotated and cropped image of dimensions [N, N] and corresponding color channels.

    """
    assert image.shape[0] == image.shape[1], "the rotated image must be of square dimensions"

    w = image.shape[0]
    lower = int(w)  # add/subtract 1 to avoid interpolation-induced black borders
    upper = int(2 * w)

    canvas = tile(image)

    rotimg = PIL.Image.fromarray(canvas).rotate(deg, resample=PIL.Image.BICUBIC)
    rotarr = np.frombuffer(rotimg.tobytes(), dtype=np.uint8).reshape(canvas.shape)

    return rotarr[lower:upper, lower:upper]

def add_pixel_noise(img, max_strength=0.3, rand=None):
    """
    Adds random noise of the given maximum strength to the
    given img.
    """
    if rand is None:
        rand = np.random.RandomState()
    img_space_noise = 255 * rand.rand(*img.shape)
    sampled_strength = rand.rand() * 0.3
    return np.clip(sampled_strength * img_space_noise + img, 0, 255).astype("uint8")
