import os
import imageio
import subprocess
import numpy as np
import re

class Dataset(object):
    def __init__(self):
        self.basepath = absolute_path("./data/")
        
        self.training_data = None
        
        self.test_data = None
        # the numbers of the test images in order of self.test_data
        # (required for submission generation)
        self.test_image_numbers = None
        
    def get_training_data(self):
        """
        Returns pairs of training samples. A pair is comprised of a numpy array representing 
        the satellite image and a numpy array representing the groundtruth segmentation
        mask.
        
        The image data is of shape (400, 400, 3) and (400, 400) respectivly.
        
        Loads the data lazily and chaches its.
        """
        if self.training_data is None:
            self.load_training_data()
        return self.training_data

    @staticmethod
    def split(dataset, validation_part=0.1, seed=None):
        """
        Splits the given dataset into a training and a validation data set
        based on the given `validation_part`

        :param dataset
            A list of training samples.

        :return:
            A pair of two lists representing the (training_dataset, validation_dataset).
        """
        if seed is not None:
            np.random.seed(seed)
            
        indices = set([v for v in np.random.randint(0, len(dataset), size=[int(len(dataset) * validation_part)])])

        # we can do a manual separation here, since we are operating
        # on plain python lists anyway (also there is always only 100 images in the dataset)
        training_data = []
        validation_data = []

        for pair, idx in zip(dataset, range(len(dataset))):
            if idx in indices:
                validation_data.append(pair)
            else:
                training_data.append(pair)

        return (training_data, validation_data)

    def get_test_data(self):
        """
        Returns the test data in terms of a list of numpy array (no groundtruth here).
        
        The satellite image data is of shape (608, 608).
        
        Loads the data lazily and chaches its.
        """
        if self.test_data is None:
            self.load_test_data()
        return self.test_data
    
    def get_test_image_numbers(self):
        if self.test_image_numbers is None:
            self.load_test_data()
        return self.test_image_numbers
    
    def load_training_data(self):
        """Actual loading routine for the training data. Use get_training_data as client. """
        sat_image_path = self.basepath + "training/images/"
        groundtruth_path = self.basepath + "training/groundtruth/"
        
        sat_image_paths = [sat_image_path + f for f in os.listdir(sat_image_path) if os.path.isfile(os.path.join(sat_image_path, f))]
        groundtruth_paths = [groundtruth_path + f for f in os.listdir(groundtruth_path) if os.path.isfile(os.path.join(groundtruth_path, f))]
        
        sat_image_arrays = map(lambda path: imageio.imread(path), sat_image_paths)
        groundtruth_arrays = map(lambda path: imageio.imread(path), groundtruth_paths)
        
        # list of sattelite images + corresponding groundtruth image
        self.training_data = list(zip(sat_image_arrays, groundtruth_arrays))
    
    def load_test_data(self):
        """Actual loading routine for the test data. Use get_test_data as client. """
        test_images_path = self.basepath + "test_images/"
        
        test_paths = [test_images_path + f for f in os.listdir(test_images_path) if os.path.isfile(os.path.join(test_images_path, f))]
        
        image_numbers = [int(re.search(r"\d+", filename).group(0)) for filename in test_paths]
        test_image_arrays = map(lambda path: imageio.imread(path), test_paths)
        
        # list of sattelite images + corresponding groundtruth image
        self.test_data = list(test_image_arrays)
        self.test_image_numbers = image_numbers

    def save_submission_file(self, submission_filename, *image_filenames):
        """
        Writes a submission CSV file based on the given list of segmentation 
        mask files 'image_filenames'.
        
        Extracts the number of the image from the mask filename, which should ideally 
        be of the format 'prediction_<imageid>.png'.
        
        :param submission_filename
            The name of the submission CSV file to write to.
        :param *image_filenames
            The list of segementation mask image files to consider 
            when generating the submissions.
        """
        import data.mask_to_submission # late import, since corresponding script may not have been downloaded yet
        mask_to_submission.mask_to_submission(submission_filename, *image_filenames)
    
    @staticmethod 
    def download():
        """
        Executes the external script that downloads and decompresses the dataset.
        """
        print("Downloading data set... (In Jupyter, check terminal output for progress)")
        os.system("./download-data.sh")



# returns directory this file (dataset.py) is stored in
def get_root():
    return os.path.dirname(os.path.abspath(__file__))
# converts the given dataset.py-relative path into an absolute path
def absolute_path(p):
    return get_root() + "/" + p