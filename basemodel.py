import numpy as np
from dataset import Dataset
import math

import matplotlib.pyplot as plt

import sklearn.metrics

class BaseModel(object):
    """
    Base class for implementing road segmentation models.
    
    Subclasses should implement their own train, predict and restore function.
    
    Based on the implementations of these methods, the methods compute_accuracy and
    compute_submission may then be used to evaluate a model and/or prepare a 
    corresponding submission file for kaggle.
    """
    
    def __init__(self):
        self.fg_threshold = 0.25

    def train(self, training_data):
        """
        Trains a model and saves it to disk.
        
        The saved, trained model can be restored in restore(), before any
        prediction is performed.
        
        :param training_data
            The data to train on. List of pairs of (400, 400, 3) satellite images
            and (400, 400) groundtruth segmentation images.
        """
        pass
    def predict(self, sat_image_data):
        """
        A mask value of 1.0 indicates a road at the given pixel 
        and 0.0 indicates no road.
        
        :param sat_image_batch
            Numpy array of satellite images (608, 608, 3) for which to predict road segmentation
            masks. Shape (dataset_size, 608, 608, 3).
        :return
            Numpy array of road segmentation masks as numpy arrays (608, 608) in the same order as 
            the given list of sat_image_data.
        """
        pass
    def restore(self):
        """
        Restore method that is called before performing prediction with this model.
        
        Restore any trained model in this method.
        """
        pass
    
    def compute_submission(self, submission_filename):
        """
        Computes the prediction results for the test data set
        and stores the resulting submission CSV file to the 
        given file location.
        """
        # load test data
        d = Dataset()
        test_data = np.array(d.get_test_data())
        test_image_numbers = d.get_test_image_numbers()
        # restore trained model and perform prediction
        self.restore()
        segmentation_masks = self.predict(test_data)
        
        self.masks_to_submission(submission_filename, test_image_numbers, segmentation_masks)

    def _compute_validation_result(self, validation_data, num_plot):
        """
        Computes the prediction results and prediction targets for the 
        given validation data set.

        :returns
            (validation_target, prediction, submission_target, submission_prediction)
        """
        self.restore()
        
        validation_input = np.array(list(map(lambda pair: pair[0], validation_data)))
        validation_target = np.array(list(map(lambda pair: pair[1], validation_data)))
        
        validation_target = validation_target / validation_target.max() # normalize segmentation masks
        assert np.all(validation_target <= 1), "the target mask should range between [0, 1]"
        assert np.all(validation_target >= 0), "the target mask should range between [0, 1]"
        
        prediction = self.predict(validation_input)

        # generate submission-like low-resolution variant of target and prediction
        submission_prediction = np.array([generate_submission_patches(p, self.fg_threshold) for p in prediction])
        submission_target = np.array([generate_submission_patches(p, self.fg_threshold) for p in validation_target])

        # plot samples if specified
        for i in range(min(num_plot, len(validation_data))):
            plt.subplot(1, 4, 1).imshow(validation_input[i])
            plt.subplot(1, 4, 2).imshow(validation_target[i], vmin=0.0, vmax=2.0)
            plt.subplot(1, 4, 3).imshow(prediction[i], vmin=0.0, vmax=2.0)

            prediction_patches = generate_submission_patches(prediction[i], self.fg_threshold)
            patches_per_side = int(math.sqrt(len(prediction_patches)))
            patched_submission = np.kron(prediction_patches.reshape(patches_per_side, patches_per_side).T, np.ones((16, 16)))

            plt.subplot(1, 4, 4).imshow(patched_submission, vmin=0.0, vmax=2.0)

            plt.show()

        return (validation_target, prediction, submission_target, submission_prediction)

    def compute_accuracy(self, validation_data, plot=0):
        """
        Computes the accuracy of this model on the given validation 
        data set. 
        
        This computes for one, a mean-squared loss for the exact prediction
        and target and secondarily for the submission-like patch-level loss.
        
        :param validation_data
            The validation data set to compute the accuracy for.
        :param plot=0
            The number of validation samples to plot including their
            original image, groundtruth and prediction
            
        :return
            (exact_loss, submission_loss)
        """
        validation_target, prediction, submission_target, submission_prediction = self._compute_validation_result(validation_data, plot)
        
        # mean squared loss
        exact_loss = np.sqrt(np.square(prediction - validation_target).mean())
        submission_loss = np.sqrt(np.square(submission_prediction - submission_target).mean())
        
        return (1.0 - exact_loss, 1.0 - submission_loss)
    
    def compute_f1_score(self, validation_data, plot=0):
        """
        Computes the F1 score of this model for the given validation data set. 

        Only computes the F1 score for discretized (binary) submission variant of the
        prediction output. 
        
        :param validation_data
            The validation data set to compute the accuracy for.
        :param plot=0
            The number of validation samples to plot including their
            original image, groundtruth and prediction (for visualization purposes).
            
        :return
            submission F1 score
        """
        _, _, submission_target, submission_prediction = self._compute_validation_result(validation_data, plot)
        
        # F1 score via scikit
        submission_f1 = sklearn.metrics.f1_score(submission_target.flatten(), submission_prediction.flatten(), average='micro')
        
        return submission_f1
        
    # adapted from mask_to_submission.py
    def mask_to_submission_strings(self, img_number, im):
        """Reads a single image and outputs the strings that should go into the submission file"""
        #img_number = int(re.search(r"\d+", image_filename).group(0))
        #im = mpimg.imread(image_filename)
        patch_size = 16
        
        # enable the following to check how the 16x16 sampling
        # affects the actual model output when creating the submission file
        
        #print("Image {}".format(img_number))
        #subm = np.array(generate_submission_patches(im)).reshape([38, 38]).T
        #plt.subplot(1, 2, 1).imshow(im)
        #plt.subplot(1, 2, 2).imshow(subm)
        #plt.show()

        for j in range(0, im.shape[1], patch_size):
            for i in range(0, im.shape[0], patch_size):
                patch = im[i:i + patch_size, j:j + patch_size]
                label = patch_to_label(patch, self.fg_threshold)
                yield("{:03d}_{}_{},{}".format(img_number, j, i, label))

    
    def masks_to_submission(self, submission_filename, img_numbers, masks):
        """Converts images into a submission file"""
        with open(submission_filename, 'w') as f:
            f.write('Id,prediction\n')
            for img_no, mask in zip(img_numbers, masks):
                f.writelines('{}\n'.format(s) for s in self.mask_to_submission_strings(img_no, mask))

                
# assign a label to a patch
def patch_to_label(patch, foreground_threshold):
    df = np.mean(patch)
    if df > foreground_threshold:
        return 1
    else:
        return 0

def generate_submission_patches(im, foreground_threshold):
    """
    Returns an array of submission labels (road/not road) per patch
    as it is generated for the submission file.
    
    Segments the image into patches of size 16x16 and computes the mean
    value for each patch. A mean value > foreground_threshold translates
    to a label of 1 or 0.
    """
    patches = []
    patch_size = 16
    for j in range(0, im.shape[1], patch_size):
        for i in range(0, im.shape[0], patch_size):
            patch = im[i:i + patch_size, j:j + patch_size]
            patches.append(patch_to_label(patch, foreground_threshold))
    return np.array(patches)
